import sys
import math
import numpy as np
import matplotlib.pyplot as plt

def read_Xi(filename, arr):
    with open(filename) as f_in:
        # Reading the first line - header
        line = f_in.readline()
        temp = line.split()[1:]
        for elem in temp:
            arr.append( [] )

        # Reading other lines
        for line in f_in:
            temp = line.split()[1:]
            i = 0
            for elem in temp:
                if elem == '-': elem = "0"
                arr[i].append( float(elem) )
                i += 1


def interp_w(T_imp, x_T, y_T, x):
    i_e = 0
    i_w = 0
    j = len(x_T[i_w]) - 1
    while x_T[i_e][j] < x:
        i_e += 1
        i_w = i_e - 1
        j = len(x_T[i_w]) - 1
    
    f_e = (x_T[i_e][j] - x)/(x_T[i_e][j] - x_T[i_w][j])
    T = f_e*T_imp[i_w][j] + (1 - f_e)*T_imp[i_e][j]
    return T
    
        

def interp(T_imp, x_T, y_T, x, y):
    i_y_T = 0
    while x_T[i_y_T][-1] < x: i_y_T += 1
    i_y_T -= 1

    j_n = 0
    j_s = 0
    while y_T[i_y_T][j_s] > y:
        j_s += 1
        j_n = j_s - 1

    i_ne = i_y_T
    i_nw = i_y_T
    i_se = i_y_T
    i_sw = i_y_T

    while x_T[i_ne][j_n] < x:
        i_ne += 1
        i_nw = i_ne - 1

    while x_T[i_se][j_s] < x:
        i_se += 1
        i_sw = i_se - 1
        
    f_ne = (x_T[i_ne][j_n] - x)/(x_T[i_ne][j_n] - x_T[i_nw][j_n])
    T_n = f_ne*T_imp[i_nw][j_n] + (1 - f_ne)*T_imp[i_ne][j_n]

    f_se = (x_T[i_se][j_s] - x)/(x_T[i_se][j_s] - x_T[i_sw][j_s])
    T_s = f_se*T_imp[i_sw][j_s] + (1 -f_se)*T_imp[i_se][j_s]

    f_n = 0
    try:
        f_n = (y_T[i_y_T][j_n] - y)/(y_T[i_y_T][j_n] - y_T[i_y_T][j_s])
    except ZeroDivisionError:
        print "ZeroDivisionError during interpolation at ", x, y
        
    
    T = f_n*T_s + (1 - f_n)*T_n
    
    return T 


def kappa_mix(X_CO2, X_H2O, X_C2H4, X_C3H6, X_C5H8O2, T):
    T_g = T
    T_p = 1.0 / T

    kappa_CO2 = ((((-5.6251e15*T_p + 5.5371e13)*T_p - 1.9608e11)*T_p + 2.9127e8)*T_p - 1.4779e5)*T_p + 2.4510e1

    kappa_H2O = ((((5.4927e14*T_p - 3.4354e12)*T_p + 7.1325e9)*T_p - 1.4267e6)*T_p + 2.9671e3)*T_p - 9.9326e-1

    kappa_C2H4 = 0
    
    kappa_C3H6 = 3.88e1 + T_g * (-6.46e-2 + T_g * (4.67e-5 + T_g * (-1.63e-8 + 2.20e-12 * T_g)))

    kappa_C5H8O2 = ((((-8.1176e13 * T_p + 9.8352e12) * T_p - 7.7764e10) * T_p + 1.8935e8) * T_p - 9.5780e4) * T_p + 1.7343e1

    
    return X_CO2 * kappa_CO2 + X_H2O * kappa_H2O + X_C2H4 * kappa_C2H4 + X_C3H6 * kappa_C3H6 + X_C5H8O2 * kappa_C5H8O2 




T_imp = []
x_T = []
y_T = []
# Reading T field
with open("T.dat") as f_in:
    # Reading the first line - header
    line = f_in.readline()
    temp = line.split()[1:]
    for i in range(len(temp)):
        if (i % 2 == 0):
            x_T.append( [] )
            y_T.append( [] )
        else: T_imp.append( [] ) 

    # Reading other lines
    for line in f_in:
        y_temp = float(line.split()[0]) 
        temp = line.split()[1:]
        for i in range(len(temp)):
            index = i / 2
            elem = temp[i]
            if elem != '-':
                if (i % 2 == 0):
                    x_T[index].append( float(elem) )
                    y_T[index].append( y_temp )
                else: T_imp[index].append( float(elem) )



x_X = []
y_X = []
# Reading X fields coordinates
with open("Y_CO2.dat") as f_in:
    # Reading the first line - header
    line = f_in.readline()
    temp = line.split()[1:]
    
    for elem in temp:
        x_X.append( float(elem) )

    # Reading other lines
    for line in f_in:
        y_X.append( float(line.split()[0]) )

        
X_CO2_imp = []
read_Xi("Y_CO2.dat", X_CO2_imp)
X_H2O_imp = []
read_Xi("Y_H2O.dat", X_H2O_imp)
X_C2H4_imp = []
read_Xi("Y_C2H4.dat", X_C2H4_imp)
X_C3H6_imp = []
read_Xi("Y_C3H6.dat", X_C3H6_imp)
X_C5H8O2_imp = []
read_Xi("Y_C5H8O2.dat", X_C5H8O2_imp)

            
# Selecting concentrations points inside temperature measured area
x = []
y = []
x_offset = -1
y_offset = []
for i in range(len(x_X)):
    y_offset.append(-1)

    if (x_X[i] > x_T[0][0] and x_X[i] < x_T[-1][-1]):
        x.append(x_X[i])
        y.append([])

        if(x_offset == -1): x_offset = i
        
        i_w = 0
        i_e = 0
        while x_T[i_e][-1] < x_X[i]:
            i_e += 1
            i_w = i_e - 1

        for j in range(len(y_X)):
            if (y_X[j] < y_T[i_w][0] and y_X[j] > y_T[i_w][-1]):
                y[-1].append(y_X[j])
                if(y_offset[i] == -1): y_offset[i] = j

X_CO2 = []
X_H2O = []
X_C2H4 = []
X_C3H6 = []
X_C5H8O2 = []
for i in range(len(x)):
    i_X = i + x_offset
    X_CO2.append( [] )
    X_H2O.append( [] )
    X_C2H4.append( [] )
    X_C3H6.append( [] )
    X_C5H8O2.append( [] )
    for j in range(len(y[i])):
        j_X = j + y_offset[i_X]
        X_CO2[-1].append( X_CO2_imp[ i_X ][ j_X ] )
        X_H2O[-1].append( X_H2O_imp[ i_X ][ j_X ] )
        X_C2H4[-1].append( X_C2H4_imp[ i_X ][ j_X ] )
        X_C3H6[-1].append( X_C3H6_imp[ i_X ][ j_X ] )
        X_C5H8O2[-1].append( X_C5H8O2_imp[ i_X ][ j_X ] )

        
                
# Zero negative concentrations
for i in range(len(x)):
    for j in range(len(y[i])):
        if X_CO2[i][j] < 0: X_CO2[i][j] = 0
        if X_H2O[i][j] < 0: X_H2O[i][j] = 0
        if X_C2H4[i][j] < 0: X_C2H4[i][j] = 0
        if X_C3H6[i][j] < 0: X_C3H6[i][j] = 0
        if X_C5H8O2[i][j] < 0: X_C5H8O2[i][j] = 0


interp(T_imp, x_T, y_T, 30, -0.035)


# Interpolate from T_imp mesh to concentrations mesh
T = []
for i in range(len(x)):
    T.append( [] )
    for j in range(len(y[i])):
        T[i].append( interp(T_imp, x_T, y_T, x[i], y[i][j]) )


# Compute surface temperature
T_w = []
for i in range(len(x)):
    T_w.append( interp_w(T_imp, x_T, y_T, x[i]) )

# Compute gass mixture kappa values
kappa = []
for i in range(len(x)):
    kappa.append( [] )
    for j in range(len(y[i])):
        kappa[i].append( kappa_mix( X_CO2[i][j], X_H2O[i][j], 0, X_C3H6[i][j], X_C5H8O2[i][j], T[i][j]) )

# Precompute underintegral part of wall radiative heat flux
Q_r = []
for i in range(len(x)):
    Q_r.append ( [] )
    for j in range(len(y[i])):
        Q_r[i].append( kappa[i][j] * (T[i][j]**4 - T_w[i]**4) )

       
# Compute wall radiative heat flux
epsilon_w = 1.0
sigma_SB = 5.67e-8
metric_scale = 0.001
q_w_r = []
for i in range(len(x)):
    q_w_r.append(0)
    for j in range(len(y[i])-1):
        q_w_r[i] += 0.5 * (Q_r[i][j] + Q_r[i][j+1]) * abs(y[i][j] - y[i][j+1]) * metric_scale
    q_w_r[i] *= 2 * epsilon_w * sigma_SB

print x
print T_w
print q_w_r

with open("q_w_r.dat","w+") as f_out:
    f_out.write("x,mm ");
    for i in range(len(x)):
        f_out.write(str(x[i]) + " ");

    f_out.write("\n");
    f_out.write("q_w_r,W/m2 ");
    for i in range(len(q_w_r)):
        f_out.write('{:12.5e}'.format(q_w_r[i]));
       


"""
with open("Y_CO2.csv","w+") as f_out:
    f_out.write("x, y, z, Y_CO2 \n")
    for i in range (len(x)):
        for j in range (len(y)):
            f_out.write(str(x[i]*metric_scale) + ", " + str(y[j]*metric_scale) + ", 0, " + str(X_CO2[i][j]) + "\n")

with open("YH2O.csv","w+") as f_out:
    f_out.write("x, y, z, Y_H2O \n")
    for i in range (len(x)):
        for j in range (len(y)):
            f_out.write(str(x[i]*metric_scale) + ", " + str(y[j]*metric_scale) + ", 0, " + str(X_H2O[i][j]) + "\n")

with open("T.csv","w+") as f_out:
    f_out.write("x, y, z, T \n")
    for i in range (len(T_imp)):
        for j in range (len(T_imp[i])):
            f_out.write(str(x_T[i][j]*metric_scale) + ", " + str(y_T[i][j]*metric_scale) + ", 0, " + str(T_imp[i][j]) + "\n")
"""
